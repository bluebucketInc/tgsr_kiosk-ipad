// imports
import React from 'react'
import { Field, reduxForm } from 'redux-form'
import { isMobileOnly } from 'react-device-detect'
// mui
import FormControl from '@material-ui/core/FormControl'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import { ThemeProvider } from '@material-ui/styles'
import TextField from '@material-ui/core/TextField'
import Checkbox from '@material-ui/core/Checkbox'
import { FormHelperText } from '@material-ui/core'
// components
import { Button } from '../button'
import { validate } from '../../utils'
// theme
import { theme, formStyles } from '../../styles/form-theme'
import './styles.scss'
// form items
const formItems = [
    {
        name: 'name',
        label: 'Name',
        error: 'Please indicate your name',
    },
    {
        name: 'email',
        label: 'Email Address',
        error: 'Please indicate your email address',
    },
    {
        name: 'age',
        label: 'Age',
        error: 'Please indicate your age',
        last: true,
    },
    {
        name: 'phone',
        label: 'Contact Number',
        error: 'Please indicate your contact number',
        last: true,
    },
]

// form
const FormPage = ({ handleSubmit, pristine, reset, submitting, invalid }) => {
    return (
        <ThemeProvider theme={theme}>
            <form onSubmit={handleSubmit} className="form-wrapper">
                <div className="container form-container">
                    <div className="row">
                        {formItems.map(item => (
                            <div
                                key={item.label}
                                className={`col-md-6 col-sm-12 col-xs-12 field-item ${
                                    item.last ? 'last' : ''
                                }`}>
                                <Field
                                    errorText={item.error}
                                    name={item.name}
                                    component={renderTextField}
                                    label={item.label}
                                />
                            </div>
                        ))}
                    </div>
                    <div className="row">
                        <div
                            className={`col-sm-12 ${
                                isMobileOnly ? 'mobile-checkbox-wrapper' : ''
                            }`}>
                            <Field
                                name="consent"
                                component={renderCheckbox}
                                label={
                                    <p>
                                        I hereby consent to the collection, use,
                                        disclosure and processing of my personal
                                        data in accordance with the TGSR{' '}
                                        <a
                                            href="/privacy.html"
                                            target="_blank"
                                            rel="noopener noreferrer">
                                            Privacy Policy
                                        </a>{' '}
                                        for the purposes of the weekly lucky
                                        draw (including informing the winner on
                                        collection of prizes).
                                    </p>
                                }
                            />
                        </div>
                    </div>
                    <div className="button-group buttons-wrapper">
                        <Button
                            classes={`long-btn ${
                                pristine || submitting ? 'disabled' : ''
                            }`}
                            onClick={reset}>
                            Clear
                        </Button>
                        <Button
                            classes={`long-btn ${
                                invalid || submitting ? 'disabled' : ''
                            }`}
                            type="submit">
                            Submit
                        </Button>
                    </div>
                </div>
            </form>
        </ThemeProvider>
    )
}

// text field
const renderTextField = ({
    input,
    label,
    meta: { touched, error },
    errorText,
    ...custom
}) => {
    const classes = formStyles()
    return (
        <FormControl className={`${classes.formControl} form-control`}>
            <TextField
                fullWidth={true}
                className={`${classes.textinput} text-input`}
                variant="filled"
                label={label}
                {...input}
                {...custom}
            />
            <FormHelperText className="error-message">
                {error && touched ? errorText : ''}
            </FormHelperText>
        </FormControl>
    )
}

// checkbox
const renderCheckbox = ({ input, label }) => (
    <FormControlLabel
        className="checkbox-wrapper"
        control={
            <Checkbox
                color="primary"
                className="checkbox"
                checked={input.value ? true : false}
                onChange={input.onChange}
            />
        }
        label={label}
    />
)

// export
export default reduxForm({
    form: 'SubmitForm',
    validate,
})(FormPage)
