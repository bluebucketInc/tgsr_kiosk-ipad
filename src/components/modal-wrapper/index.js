/*global $*/

// imports
import React, { useEffect } from 'react'
import { isMobile, isTablet } from 'react-device-detect'
import { Link } from 'react-router-dom'
import Rodal from 'rodal'

// include styles
import './styles.scss'

// component
const ModalWrapper = ({ isActive, onClose, children, to, className }) => {
    // change
    useEffect(() => {
        isActive
            ? $('body').addClass('popup-active')
            : $('body').removeClass('popup-active')
    }, [isActive])

    // render
    return (
        <Rodal
            width={100}
            height={100}
            measure="%"
            visible={isActive}
            onClose={onClose}
            closeMaskOnClick={false}
            animation="fade"
            leaveAnimation="slideDown"
            duration={600}
            className={`modal-wrapper ${className}`}
            showCloseButton={false}>
            {/* <div className="tgsr-logo">
                <a href="/">
                    <img
                        alt="TGSR"
                        src="/clientlibs/images/global/tgsr_logo.gif"
                    />
                </a>
            </div> */}
            {to ? (
                <Link to="/" onClick={onClose} style={{ position: 'initial' }}>
                    {isMobile || isTablet ? (
                        <img
                            src={require(`./assets/close-btn-mobile.png`)}
                            alt="close button"
                            className="close-btn"
                        />
                    ) : (
                        <img
                            src={require(`./assets/close-btn.png`)}
                            alt="close button"
                            className="close-btn"
                        />
                    )}
                </Link>
            ) : isMobile || isTablet ? (
                <img
                    onClick={onClose}
                    src={require(`./assets/close-btn-mobile.png`)}
                    alt="close button"
                    className="close-btn"
                />
            ) : (
                <img
                    onClick={onClose}
                    src={require(`./assets/close-btn.png`)}
                    alt="close button"
                    className="close-btn"
                />
            )}
            {children}
        </Rodal>
    )
}

// export
export default ModalWrapper
