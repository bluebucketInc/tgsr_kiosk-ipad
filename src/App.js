// imports
import React from 'react'
import { Route } from 'react-router-dom'

// pages
import BoomBox from './containers/boombox'
import VoteOverlay from './containers/vote-overlay'
import OnboardingScreens from './containers/onboarding'
import { WhyVote, WinPrizes } from './containers/content-popup'


// app
export default class App extends React.Component {
    // render method
    render() {
        return (
            <React.Fragment>
                <Route
                    exact
                    path="/"
                    component={BoomBox}
                />
                <Route exact path="/vote" component={VoteOverlay} />
                <Route exact path="/why-vote" component={WhyVote} />
                <Route exact path="/how-to-win" component={WinPrizes} />
                <Route exact path="/how-to-play" component={OnboardingScreens} />
            </React.Fragment>
        )
    }
}
