// onboarding screens
export const onBoarding = [
    {
        sub: 'CREATE YOUR MASHUP AND VOTE',
        head: 'STEP 1: PICK A TIME. PICK A TUNE.',
        image: require('../assets/images/onboarding/one.png'),
        image_d: require('../assets/images/onboarding/one_desktop.png'),
        firstSlide: true,
        video: {
            tablet: require('../assets/videos/onboarding/1_tablet.mp4'),
            desktop: require('../assets/videos/onboarding/1_desktop.mp4'),
            mobile: require('../assets/videos/onboarding/mobile/1.mp4'),
        },
    },
    {
        sub: 'CREATE YOUR MASHUP AND VOTE',
        head: 'STEP 2: LISTEN TO YOUR MASHUP',
        image: require('../assets/images/onboarding/two.png'),
        image_m: require('../assets/images/onboarding/mobile/two.png'),
        video: {
            desktop: require('../assets/videos/onboarding/2.mp4'),
            mobile: require('../assets/videos/onboarding/mobile/2.mp4'),
        },
    },
    {
        sub: 'CREATE YOUR MASHUP AND VOTE',
        head: 'STEP 3: VOTE FOR YOUR FAVOURITE MASHUP',
        image: require('../assets/images/onboarding/three.png'),
        image_m: require('../assets/images/onboarding/mobile/three.png'),
        video: {
            desktop: require('../assets/videos/onboarding/3.mp4'),
            mobile: require('../assets/videos/onboarding/mobile/3.mp4'),
        },
    },
]
