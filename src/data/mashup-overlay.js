// onboarding screens
export const overlay = {
    '00': {
        image: require('../assets/images/overlay/00.png'),
        sub: '1300s x Pop Music',
        para:
            'As the night in 14th Century Temasek descends, imagine gamelan gongs and angklung sounds start to fall into the rhythm of your pop beat. Feel it push your tune along with the sound of the water in the cool night air. ',
    },
    '01': {
        image: require('../assets/images/overlay/01.png'),
        sub: '1300s x Indie Music',
        para:
            'Go for a hike in the jungle in early Temasek, leaving the trading post at the coast. Play your Indie mashup and watch the forest come alive, with howler monkeys singing along to the music. An Orang Laut flute plays along with your rock tune and the jungle moves with you, wild and free.',
    },
    '02': {
        image: require('../assets/images/overlay/02.png'),
        sub: '1300s x Jazz Music',
        para:
            'Cue crickets chirping in the night by the edge of the Temasek inlands. Together with a Jazz saxophone, flutes and gamelan gongs of the time play along, adding a flair and alluring mystery to this early trading post waiting to be discovered. ',
    },
    '03': {
        image: require('../assets/images/overlay/03.png'),
        sub: '1300s x Electronic Music ',
        para:
            'Imagine a rave in the 1300s in a young Temasek, celebratory sounds soaring from the jungle near the coast. Listen to the people of that time reminiscing around a fire – the Orang Laut singing about their sails and travels, while playing their traditional instruments like gamelan gongs. Enjoy the beat as it sweetens and grows.',
    },
    '04': {
        image: require('../assets/images/overlay/04.png'),
        sub: '1300s x Urban Music',
        para:
            'As the night in 14th Century Temasek descends, imagine gamelan gongs, angklung and flute sounds start to fall into the rhythm of your pop beat. Feel the flow push your tune along with the sound of the water in the cool night air. ',
    },
    '10': {
        image: require('../assets/images/overlay/10.png'),
        sub: '1965-Present x Pop Music',
        para:
            'Enter the boom of a jet set on its course. Listen to the bells of the MRT chime. Then, ringtones of smartphones go off as these various modern pop sounds come together naturally with the environment of the modern age.',
    },
    '11': {
        image: require('../assets/images/overlay/11.png'),
        sub: '1965-PRESENT x INDIE Music',
        para:
            'The beat of progress plays louder than ever as Singapore’s economy, infrastructure and technology grow exponentially. More contemporary sounds start to fill the airwaves – planes and trains, keyboards and mobile phones. And as our independence takes shape, so does our local indie music scene.',
    },
    '12': {
        image: require('../assets/images/overlay/12.png'),
        sub: '1965-PRESENT x JAZZ Music',
        para:
            'Transitioning smoothly into progress, like the switching basslines of a modern jazz song, Singapore is humming a different tune. The sound of planes, smartphones, and gadgets click and beep everywhere. Then, busy streets and buskers in the distance create a more polished nightlife charm that blend the old and the new. ',
    },
    '13': {
        image: require('../assets/images/overlay/13.png'),
        sub: '1965-PRESENT x ELECTRONIC Music',
        para:
            'A time of progress by leaps and bounds since our independence. Take an audio journey through the iconic Merdeka speech and into becoming a major trading hub. Hear the bleeps of electronic devices and dial-up modems blending with the beat, while the typing sound of a laptop keyboard fits into the rhythm of the song as added percussion.',
    },
    '14': {
        image: require('../assets/images/overlay/14.png'),
        sub: '1965-Present x Urban Music',
        para:
            'Press play into Singapore’s modern era – an economically and technologically sound time. Hear a sample of the iconic “Merdeka”, which signified our independence and destiny as a nation. And let the hip-hop nature of the beat move you as we progress into a truly modern, urban Singapore.',
    },
    '20': {
        image: require('../assets/images/overlay/20.png'),
        sub: '1900-1945 x Pop Music',
        para:
            'A more crowded pre-war Singapore is what you hear. A sophisticated sound – jazz music on a gramophone pulses with silverware clinking, private telephones ringing; all this while towards the end, the horns blaring from a big band synchronises with your pop rhythm.',
    },
    '21': {
        image: require('../assets/images/overlay/21.png'),
        sub: '1900-1945 x Indie Music',
        para:
            'Singapore starts singing to a more populated and cosmopolitan tune. Early 1900 city-life mirrors many big cities in the world, where entertainment rocks the streets after working hours. Hear the hustle and bustle graduate into a big band fronted by a Shanghai jazz crooner. Listen closely and you’ll hear the reflection of swing dancing at the many famous clubs and cabarets in this mashup.',
    },
    '22': {
        image: require('../assets/images/overlay/22.png'),
        sub: '1900-1945 x Jazz Music',
        para:
            'Singapore reaches its pre-war peak in the early 1900s. Soldiers marching in rhythm and gramophones crackling are familiar sounds. Listen to the enjoyable chatter and mingling of the working class in a Shanghai jazz club as a crooner sings and a saxophone player takes a fully improvised solo.',
    },
    '23': {
        image: require('../assets/images/overlay/23.png'),
        sub: '1900-1945 x Electronic Music',
        para:
            'It’s the height of Singapore’s pre-war status under British rule, a more developed nation with loud bustling ports and busy streets, with the first automobiles in tow. The air of that time is filled with the sounds of Shanghai Jazz from Club Street, gramophones and chatter from social clubs. Listen as a Chinese jazz singer and nightclub big band falls in line to your electronic sound. ',
    },
    '24': {
        image: require('../assets/images/overlay/24.png'),
        sub: '1900-1945 x Urban Music',
        para:
            'Life at the turn of the 20th Century increases in pace. You hear vintage automobile horns as they drive by the busy streets of Singapore. The fast-paced Gibraltar of the East gathers a reputation for itself as Singapore becomes a British Crown Jewel. The sounds of local social clubs and cabarets is that of early vernacular and Shanghai Jazz music that mix effortlessly with your urban beats.',
    },
    '30': {
        image: require('../assets/images/overlay/30.png'),
        sub: '1819 x Pop Music',
        para:
            'The disciplined trot of a horse moves with your music as you take in the sights and the sounds of British Singapore at midday. A searing violin solo gloriously engulfs the atmosphere, high and above the chimes of church bells. The mood gives you a purpose; a fresh musical start in a day in British Singapore.',
    },
    '31': {
        image: require('../assets/images/overlay/31.png'),
        sub: '1819  x Indie Music',
        para:
            'As Singapore awakens, you hear the bustle of development under new British Governance. Horse-trots match the cymbals of the drums of your jazz tune. Then, a virtuoso classical violin sharply, neatly and strongly plays while church bells chime, adding a contrast of order to the freedom of expression that jazz warrants from its improvisational nature. ',
    },
    '32': {
        image: require('../assets/images/overlay/32.png'),
        sub: '1819 x Jazz Music',
        para:
            'As Singapore rises, you hear the bustle of development under new British Governance. Church bells are prevalent. Horse-trots match the cymbals of the drums of your jazz tune. Then, a virtuoso classical violin sharply, neatly and strongly plays, adding a contrast of order to the freedom of expression that jazz warrants from its improvisational nature.',
    },
    '33': {
        image: require('../assets/images/overlay/33.png'),
        sub: '1819 x Electronic Music ',
        para:
            'Make your way through early 19th-century Singapore, padded with cobbled streets and its new western architecture. The Revere Bell signals curfews in these dangerous nights. Hear the hooves of horses trot in time to your music, as a concert violinist begins to play, adding a stylish accent to the beat.',
    },
    '34': {
        image: require('../assets/images/overlay/34.png'),
        sub: '1819 x Urban Music',
        para:
            'The tempo is set with the influx of interest and wealth from all parts of the greater region with Asian, Arabic and European pioneers settling on the little island in the East Indies. The march of horses is the beat that ushers in the industrial age in British Singapore. Bells are distinct and widespread. Western influences in governance and culture become the notes imprinted on society, much like a violin solo in a grand orchestral piece.',
    },
    '40': {
        image: require('../assets/images/overlay/40.png'),
        sub: '1400-1700 x Pop Music',
        para:
            'You stroll through the morning market, the sound of birds chirping and children playing fill the air. A holy man chants and his spiritual melody weaves in and out of the modern pop beat. The kick of tablas fuels your beat as you move into the city square. Then, the echo of a rubab (Afghan lute-like instrument) with the royal nobat orchestra is a welcome addition to the vibe.',
    },
    '41': {
        image: require('../assets/images/overlay/41.png'),
        sub: '1400-1700 x Indie Music',
        para:
            'Step to the beat of the morning market. Hear a cock crowing in the distance. Early guitar-like instruments like the Er Hu and Pipa strum along, while the market hums with people trading goods. Listen closely as the down-tuned crowds loop and the Nobat’s nafiri (trumpet-like royal court instrument) starts to toot with your beat as you go through the busy shahbandaria.',
    },
    '42': {
        image: require('../assets/images/overlay/42.png'),
        sub: '1400-1700 x Jazz Music',
        para:
            'Sounds of sunset in the markets, and fortifications of the 15-16th century kingdom of Singapura. As you hear these activities, the tune of a Pipa and Er Hu improvise loosely and creatively along with your noir jazz beat. Plus, the nafiri (trumpet-like bamboo instrument) playing alongside the Nobat (royal orchestra) comes in, as they did for the early royals of the time.',
    },
    '43': {
        image: require('../assets/images/overlay/43.png'),
        sub: '1400-1700 x Electronic Music',
        para:
            'You walk through a 16th-century bazaar, the marketplace sounds start to pulse along to a steady house beat. As you near royal courts and temples, a Nobat’s nafiri (trumpet-like bamboo instrument) and Indian tablas playing find your ears. At the same time, the drones of Pipa and Portuguese lute’s plucking accompany an electronic synthesiser’s solo.',
    },
    '44': {
        image: require('../assets/images/overlay/44.png'),
        sub: '1400-1700 x Urban Music',
        para:
            'It’s a musical journey in a market full of colour and commerce. Listen to a Buddhist gong and a rooster crowing. Then, feel the tablas synchronising with religious men singing, next to the Arabic Oud (lute), Pipa (Chinese Lute) and nafiri. All is blending seamlessly just like the areas of trade and temples of that time.',
    },
}
