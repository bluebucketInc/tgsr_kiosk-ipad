// imports
import React from 'react'
import { Link } from 'react-router-dom'
// components
import FadeInWrapper from '../../components/fadein-wrapper'
import SubmitForm from '../../components/form'
import { Button } from '../../components/button'
import { IndextoGenre } from '../../utils'
import { useCookies } from 'react-cookie'

// styles
import './styles.scss'

// ReCaptcha
export const ReCaptchaPage = ({
    left,
    right,
    onVoteSubmit,
    onCaptchaComplete,
    isCaptchaVerified,
    Cookies,
}) => {
    // current selection
    const selection = 'tg_' + left + '' + right
    // cookie hook
    const [cookies, setCookie, removeCookie] = useCookies([selection])
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h2>AWESOME CHOICE! YOU’RE VOTING FOR</h2>
            <div className="genre-wrapper">
                <h1>{IndextoGenre(left, 'left')}</h1>
                <img
                    className="genre-x"
                    src={require('./assets/close.png')}
                    alt="Mashup"
                />
                <h1>{IndextoGenre(right, 'right')}</h1>
            </div>
            <Button
                classes="long-btn"
                onClick={() => {
                    let currentValue = parseInt(cookies[selection])
                    // new value
                    const newValue =
                        !currentValue || currentValue < 1 ? 1 : currentValue + 1
                    // remove first
                    removeCookie(selection, {
                        path: '/',
                        maxAge: 60 * 60 * 24 * 365 * 10,
                    })
                    // set cookie
                    setCookie(selection, newValue, {
                        path: '/',
                        maxAge: 60 * 60 * 24 * 365 * 10,
                    })
                    // submit vote
                    onVoteSubmit(left, right)
                }}>
                Submit
            </Button>
        </FadeInWrapper>
    )
}

// Social Share
export const SharePage = ({ left, right, onShare, onClose }) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h2>THANK YOU FOR YOUR VOTE!</h2>
            <div className="genre-wrapper">
                <h1>{IndextoGenre(left, 'left')}</h1>
                <img
                    className="genre-x"
                    src={require('./assets/close.png')}
                    alt="Mashup"
                />
                <h1>{IndextoGenre(right, 'right')}</h1>
            </div>
            <p>
                Share your mashup to unlock your chances to win amazing prizes!
            </p>
            <p>
                <strong>
                    Should you win the weekly draw, you will be required to show
                    proof of posting at the point of prize collection.
                </strong>
            </p>
            <div className="button-group buttons-wrapper">
                <Button classes="long-btn" onClick={() => onShare('fb')}>
                    SHARE ON FACEBOOK
                </Button>
                <Button classes="long-btn" onClick={() => onShare('tw')}>
                    SHARE ON TWITTER
                </Button>
            </div>
            <Link to="/" className="link-para" onClick={onClose}>
                Create New Mashup
            </Link>
            <div className="scratchcard-container">
                <img
                    className="genre-bg"
                    src={require(`./assets/bg/${right}.png`)}
                    alt="TGSR"
                />
                <img
                    src={require(`./assets/scratch-card-locked.png`)}
                    alt="Scratch & Win"
                />
            </div>
        </FadeInWrapper>
    )
}

// Draw Page
export const DrawPage = ({ left, right, onNextStep, onClose, chances }) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h2>CONGRATULATIONS ON VOTING FOR YOUR FAVOURITE MASHUP</h2>
            <div className="genre-wrapper">
                <h1>{IndextoGenre(left, 'left')}</h1>
                <img
                    className="genre-x"
                    src={require('./assets/close.png')}
                    alt="Mashup"
                />
                <h1>{IndextoGenre(right, 'right')}</h1>
            </div>
            <p>
                You've unlocked {chances} chance({chances > 1 ? 's' : ''}) to
                win in this week's draw. Enter the draw to redeem your chances.
            </p>
            <div className="buttons-wrapper">
                <Button classes="long-btn" onClick={() => onNextStep(4)}>
                    ENTER THE DRAW
                </Button>
            </div>
            <Link to="/" className="link-para" onClick={onClose}>
                Create New Mashup
            </Link>
            <div className="scratchcard-container">
                <img
                    className="genre-bg"
                    src={require(`./assets/bg/${right}.png`)}
                    alt="TGSR"
                />
                <img
                    src={require(`./assets/scratch-card-unlocked_${chances}.png`)}
                    alt="Scratch & Win"
                />
            </div>
        </FadeInWrapper>
    )
}

// form
export const FormPage = ({ onSubmit, chances }) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h3 className="form-title">CONGRATULATIONS!</h3>
            <p>
                You’ve received {chances} chances to win $100 GV voucher in this
                week’s draw!
                <br />
                To enter the draw, simply submit your details. To enter the
                draw, submit your details. Winners will be contacted via the
                details provided.
            </p>
            <p>
                <strong>
                    Should you win the weekly draw, you will be required to show
                    proof of posting at the point of prize collection.
                </strong>
            </p>
            <SubmitForm onSubmit={onSubmit} />
        </FadeInWrapper>
    )
}

// Thank you
export const ThankYouKiosk = ({ left, right, onClose }) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h2>THANK YOU FOR YOUR VOTE!</h2>
            <div className="genre-wrapper">
                <h1>{IndextoGenre(left, 'left')}</h1>
                <img
                    className="genre-x"
                    src={require('./assets/close.png')}
                    alt="Mashup"
                />
                <h1>{IndextoGenre(right, 'right')}</h1>
            </div>
            <p>
                You’ve voted for {IndextoGenre(left, 'left')} x{' '}
                {IndextoGenre(right, 'right')}. Simply approach any of our staff
                to
                <br />
                answer a simple quiz and redeem a limited edition #TGSR gift.
            </p>
            <div className="buttons-wrapper">
                <Link to="/" onClick={onClose}>
                    <Button classes="long-btn" onClick={onClose}>
                        create new mashup
                    </Button>
                </Link>
            </div>
            <div className="scratchcard-container">
                <img
                    src={require('./assets/scratch-card-locked.png')}
                    alt="Scratch & Win"
                />
            </div>
        </FadeInWrapper>
    )
}

// Thank you
export const ThankYouDesktop = ({ left, right, onClose }) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h3 className="form-title">GOOD LUCK!</h3>
            <p>
                We will get in touch if you're one of the three lucky winners.
                <br />
                Meanwhile, why not create more mashups to get more chances!
            </p>
            <div className="buttons-wrapper">
                <Link to="/" onClick={onClose}>
                    <Button classes="long-btn" onClick={onClose}>
                        create new mashup
                    </Button>
                </Link>
            </div>
        </FadeInWrapper>
    )
}

// error page
export const ErrorPage = ({ onClose }) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h3 className="form-title">WHOOPS!</h3>
            <p>Something went wrong. Please try again later.</p>
            <div className="buttons-wrapper">
                <Link to="/" onClick={onClose}>
                    <Button classes="long-btn" onClick={onClose}>
                        Back to home
                    </Button>
                </Link>
            </div>
        </FadeInWrapper>
    )
}
