// imports
import React from 'react'
import { IndextoGenre } from '../../utils'
import { Button } from '../../components/button'

import './styles.scss'

// analyics
export const Analytics = ({ data, onStatsTrigger }) => {
    // filtered
    let filtered = []

    // items
    const items = Object.keys(data)
        .filter(key => key.includes('tg_'))
        .reduce((obj, key) => {
            obj[key] = data[key]
            return obj
        }, {})

    // rename the items
    Object.keys(items).map(item => {
        let left = IndextoGenre(parseInt(item.replace('tg_', '')[0]), 'left')
        let right = IndextoGenre(parseInt(item.replace('tg_', '')[1]), 'right')
        return filtered.push({
            [`${left} x ${right}`]: items[item],
        })
    })
    return (
        <div className="analytics-page">
            <table className="table">
                <tbody>
                    {filtered.map((item, key) => (
                        <tr key={item + key}>
                            <td>{Object.keys(item)[0]}</td>
                            <td>{item[Object.keys(item)[0]]}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
            <Button onClick={() => onStatsTrigger(false)}>CLOSE</Button>
        </div>
    )
}
