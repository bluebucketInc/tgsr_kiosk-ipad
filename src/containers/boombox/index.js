// imports
import React from 'react'
import { isMobileOnly } from 'react-device-detect'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { useCookies } from 'react-cookie'
import {
    onPlay,
    onPause,
    onSpinStart,
    onSpinEnd,
    onSelectLeft,
    onSelectRight,
    onPlayRandom,
    onInitRandom,
    onMashupOverlay,
    onGenreClick,
    onStatsTrigger,
} from '../../state/reducers/player'

// components
import Records from '../../components/records'
import PlayerLabel from '../../components/player-label'
import PlayerHelpers from '../../components/player-helpers'
import PlayerControls from '../../components/player-controls'
import FadeInWrapper from '../../components/fadein-wrapper'
import MashupOverlay from '../../containers/mashup-overlay'
import { Analytics } from '../analytics'

// styles
import './styles.scss'

// component
const BoomBox = props => {
    // cookie hook
    const [cookies, setCookie, removeCookie] = useCookies(['00'])

    return (
        <div className={`container-fluid ${isMobileOnly ? 'mobile' : ''}`}>
            {/* Stage Pattern */}
            <div
                className="clicker-container"
                onClick={() => props.onStatsTrigger(true)}></div>

            {props.isAnalyticsActive && (
                <Analytics
                    onStatsTrigger={props.onStatsTrigger}
                    data={cookies}
                />
            )}

            <div
                className={`inner-container animated fadeIn ${
                    isMobileOnly ? 'mobile-state' : ''
                }`}>
                {/* Stage Pattern */}
                <img
                    className="img-fluid stage-pattern"
                    src={require('./assets/stage-pattern.png')}
                    alt="BoomBox"
                />

                {/*popup*/}
                <MashupOverlay
                    isActive={props.popup.mashupOverlay}
                    onTrigger={props.onMashupOverlay}
                />

                {/* Boom Box */}
                <div
                    className={`boombox-container ${
                        isMobileOnly ? 'mobile-state' : ''
                    }`}>
                    {/* BoomBox Base */}
                    {isMobileOnly ? (
                        <img
                            className="img-fluid base"
                            src={require('./assets/boom-box-base_mobile.png')}
                            alt="BoomBox"
                        />
                    ) : (
                        <img
                            className="img-fluid base"
                            src={require('./assets/boom-box-base.png')}
                            alt="BoomBox"
                        />
                    )}
                    {/* Header Text */}
                    <div className="header-container">
                        <h5>PICK A TIME. PICK A TUNE.</h5>
                        <h3>CREATE YOUR MASHUP AND VOTE</h3>
                    </div>

                    {/* player label */}
                    <PlayerLabel
                        isPlaying={props.isPlaying}
                        selection={props.selection}
                        isSpinning={props.isSpinning}
                        isReady={props.isReady}
                        onLearnMore={props.onMashupOverlay}
                    />
                    {/* controls */}
                    <PlayerControls
                        isPlaying={props.isPlaying}
                        onPlay={props.onPlay}
                        onPause={props.onPause}
                        onRandom={props.onPlayRandom}
                        disableRandom={props.disableRandom}
                    />

                    {/* records */}
                    <Records
                        isPlaying={props.isPlaying}
                        onSpinStart={props.onSpinStart}
                        onSpinEnd={props.onSpinEnd}
                        onSelectLeft={props.onSelectLeft}
                        onSelectRight={props.onSelectRight}
                        isInitialLoad={props.isInitialLoad}
                        onRandom={props.onInitRandom}
                        selection={props.selection}
                        isSpinning={props.isSpinning}
                        onGenreClick={props.onGenreClick}
                        isButtonBusy={props.disableRandom}
                    />

                    {/* helpers */}
                    <PlayerHelpers
                        hasPlayed={props.selection.hasPlayed}
                        left={props.selection.left}
                        right={props.selection.right}
                    />

                    {/* Activate Lights*/}
                    <div className="neon-container">
                        <FadeInWrapper
                            isVisible={props.isPlaying || props.isInitialLoad}>
                            <img
                                className="mashup-neon flash-infinite animated"
                                src={require('./assets/mashup-light.png')}
                                alt="Mashup"
                            />
                        </FadeInWrapper>
                    </div>
                </div>
            </div>
        </div>
    )
}

// mapping state to props
const mapStateToProps = ({ player }) => ({
    isReady: player.isReady,
    isPlaying: player.isPlaying,
    isSpinning: player.isSpinning,
    selection: player.selection,
    isInitialLoad: player.isInitialLoad,
    popup: player.popup,
    disableRandom: player.isButtonBusy,
    isAnalyticsActive: player.isAnalyticsActive,
})

// dispatch to props
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            onPlay,
            onPause,
            onPlayRandom,
            onSpinStart,
            onSpinEnd,
            onSelectLeft,
            onSelectRight,
            onInitRandom,
            onMashupOverlay,
            onGenreClick,
            onStatsTrigger,
        },
        dispatch
    )

// export
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BoomBox)
