// polyfill
import 'react-app-polyfill/ie11'

// imports
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { HashRouter as Router } from 'react-router-dom'
import { CookiesProvider } from 'react-cookie'

// components
import store from './state/store'
import App from './App'

// styles
import 'sanitize.css/sanitize.css'
import './styles/app.scss'

// render method
render(
    <Provider store={store}>
        <CookiesProvider>
            <Router hashType="noslash">
                <App />
            </Router>
        </CookiesProvider>
    </Provider>,
    document.querySelector('#root')
)

// <BrowserRouter basename={'/kiosk-ipad'}>
